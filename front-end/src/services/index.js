import Axios from 'axios'
import 'axios-progress-bar/dist/nprogress.css'
import { loadProgressBar } from 'axios-progress-bar'

const axiosInstance = Axios.create({
  baseURL: 'http://localhost:3000',
  withCredentials: true
})
loadProgressBar({}, axiosInstance)
axiosInstance.interceptors.response.use(response=>response, ()=>document.getElementById('nprogress').remove() )
export default axiosInstance