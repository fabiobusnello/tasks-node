const knex = require('../../../data-base')

const _list = async (req, res) => {
  try {
    let { start, limit } = req.params.start && req.params.limit && (req.params.limit - req.params.start === 10) ? req.params : { start: 0, limit: 10 }
    const data = await knex('tasks_items')
      .select('tasks_items.id as item_id',
        'tasks_items.name as item_name',
        'tasks_items.created_at as item_created_at',
        'tasks.name as task_name',
        'tasks.id as task_id',
        'tasks.created_ad as task_created_at')
      .join('tasks', 'tasks.id', 'tasks_items.task_id').limit(Number(limit)).offset(Number(start))

    res.json({ status: true, data })
  } catch (error) {
    errors(error, res)
  }
}

module.exports = _list
