const knex = require('../../../data-base')

const _create = async (req, res) => {
  try {
    const { data } = req.body

    const { task_id } = req.params

    if (!data || !task_id) throw `Data and task_id not found`

    const [id] = await knex('tasks_items').insert({ name: data.name, task_id })

    data.id = id

    res.json({ status: true, task_item: data })

  } catch (error) {

    errors(error, res)

  }
}

module.exports = _create
