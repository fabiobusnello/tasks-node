const express = require('express')
const router = express.Router()
const actions = require('./actions')
const loadFields = require('../../helpers/load-fields')

/* GET tasks_items */
router.get('/:start?/:limit?', actions.list)

/* NEW tasks_items */
router.post('/new/:task_id', loadFields, actions.create)

/* UPDATE One tasks_items */
router.put('/update-name/:id/:name', actions.update)

/* DELETE One tasks_items */
router.delete('/remove/:id', actions.delete)

module.exports = router
