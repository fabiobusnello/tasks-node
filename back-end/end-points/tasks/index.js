const express = require('express')
const router = express.Router()
const actions = require('./actions')
const loadFields = require('../../helpers/load-fields')

/* GET One task */
router.get('/:id', actions.listTaskAndItems)

/* GET ALL tasks */
router.get('/:start?/:limit?', actions.list)

/* NEW task */
router.post('/new', loadFields, actions.create)

/* UPDATE One task */
router.put('/update/:id/:name', actions.update)

/* DELETE One task */
router.delete('/remove/:id', actions.delete)

module.exports = router
