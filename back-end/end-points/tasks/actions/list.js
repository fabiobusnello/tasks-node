const knex = require('../../../data-base')

const _list = async (req, res) => {
  try {
    let { start, limit } = req.params.start && req.params.limit ? req.params: {start: 0, limit: 10}
    const data = await knex('tasks')
    .select('id',
    'name',
    knex.raw('(SELECT count(*) as total FROM tasks) AS total, DATE_FORMAT(created_at, "%Y-%m-%d %H:%m:%s") as created_at'))
    .limit(Number(limit))
    .offset(Number(start))
    
    res.json({ status: true, data})
  } catch (error) {
    errors(error, res)
  }
}

module.exports = _list
