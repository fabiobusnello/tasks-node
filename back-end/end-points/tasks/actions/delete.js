const knex = require('../../../data-base')

const _delete = async (req, res) => {
  try {
    const { id } = req.params
    if (!id) throw `id not found!`
    await knex('tasks').delete().where({ id })
    res.json({ status: true })
  } catch (error) {
    errors(error, res)
  }
}

module.exports = _delete
