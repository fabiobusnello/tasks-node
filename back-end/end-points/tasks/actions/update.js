const knex = require('../../../data-base')

const _update = async (req, res) => {
  try {
    let { id, name } = req.params
    if(!id || !name)throw `${__filename} \n Name and id not found`
    await knex('tasks').update({name}).where({id})
    
    res.json({ status: true})
  } catch (error) {
    errors(error, res)
  }
}

module.exports = _update
