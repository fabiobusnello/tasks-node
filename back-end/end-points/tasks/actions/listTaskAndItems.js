const knex = require('../../../data-base')

const _list = async (req, res) => {
  try {
    let { id } = req.params
    let data = await knex('tasks').first('id', 'name', 'created_at').where({ id })
    if (data) data.items = await knex('tasks_items').select('id', 'name', 'created_at').where({ task_id: id })
    res.json({ status: true, data })
  } catch (error) {
    errors(error, res)
  }
}

module.exports = _list
