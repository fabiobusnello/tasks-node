/*
  auto load of actions, return example: {create, delete, list, update} according to file names

  use:
  const actions = require ('./actions')
  actions.create (req, res)
*/
const fs = require('fs')
const path = require('path')
const actions = fs.readdirSync(path.join(__dirname, './'))

const act = {}

for (const action of actions) {
  const methodName = action.replace('.js', '')
  if (methodName !== 'index' && action.includes('.js')) {
    act[methodName] = require(`./${action}`)
  }
}
module.exports = act