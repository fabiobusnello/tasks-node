const knex = require('../../../data-base')

const _create = async (req, res) => {
  try {
    const { task } = req.body
    if (!task) throw `Task not found`
    const [id] = await knex('tasks').insert({ name: task.name })
    const [{ total }] = await knex('tasks').count('id as total')
    task.id = id
    task.total = total
    res.json({ status: true, task })
  } catch (error) {
    errors(error, res)
  }
}

module.exports = _create
