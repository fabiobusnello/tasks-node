const multer = require('multer')
const express = require('express')

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
      cb(null, 'uploaded-files/tmp')
  },
  filename: (req, file, cb) => {
    if(['jpg','jpeg','png', 'octet-stream'].includes(file.mimetype.split('/')[1].toLocaleLowerCase())){
        cb(null, Date.now()+"_"+file.originalname )
    }else{
        cb('unsupported extension', null)
    } 
  }
})

// só aceita arquivos com no máximo 5mb
const upload = multer({ storage, limits: {files: 1, fileSize: 5120000} }).any()
const loadFields = async (req, res, next)=>{
  if(!req.headers['content-type'])return next()

  const contentTipe1 = req.headers['content-type'].includes('application/xml')
  || req.headers['content-type'].includes('text/plain')
  || req.headers['content-type'].includes('application/javascript')
  || req.headers['content-type'].includes('text/xml')
  || req.headers['content-type'].includes('text/html')
  
  if(contentTipe1)return next()

  const contentTipe2 = req.headers['content-type'].includes('application/x-www-form-urlencoded')
  || req.headers['content-type'].includes('application/json')

  if(contentTipe2){
    express.json()(req, res, next)
    return
  }

  upload(req, res, next, (err)=>{
    if(err){
      errors(err, res)
    }
    next()
  })
}

module.exports = loadFields