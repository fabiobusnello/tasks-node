
const fs = require('fs')
const path = require('path')
const modules = fs.readdirSync(path.join(__dirname, '../end-points'))

module.exports = app => {
  for (const mod of modules) {
    const loadModule = require(`../end-points/${mod}`)
    app.use(`/${mod}`, loadModule)
  }
}
