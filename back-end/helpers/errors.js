const fs = require('fs')
const moment = require('moment')
const errors = (err, res) => {
  let fileName = ''
  if (typeof err === 'object' && err.fileName) {
    fileName = err.fileName
  }
  if (!err) return false
  const logName = moment().format('DD_MM_YYYY-HH_mm_ss_SSS') + '.txt'
  if (err === "401" && res) {
    res.status(401).json({ status: false, message: "Permission denied!" })
  }
  if (res && err !== "401") {
    res.status(500).json({ status: false, message: "Bad request" })
  }
  const error = err.stack || err
  fs.writeFile(__dirname + '/../logs/errors/' + logName, __filename + '\n' + error, err => {
    if (err) return console.log(err)
  })
}

module.exports = errors
